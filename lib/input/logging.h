/*
    SPDX-FileCopyrightText: 2021 Roman Gilg <subdiff@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/
#pragma once

#include "kwin_export.h"

#include <QDebug>
#include <QLoggingCategory>

KWIN_EXPORT Q_DECLARE_LOGGING_CATEGORY(KWIN_INPUT)
