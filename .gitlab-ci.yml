# SPDX-FileCopyrightText: 2023 Roman Gilg <subdiff@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
stages:
  - Compliance
  - Build
  - Analysis
  - Deploy

variables:
  IMAGE_BASE: ${CI_REGISTRY}/kwinft/ci-images/archlinux/kwinft-base
  IMAGE_VERSION: master
  IMAGE: ${IMAGE_BASE}-${IMAGE_VERSION}:latest

workflow:
  rules:
    - when: always

include:
  - project: kwinft/tooling
    ref: master
    file:
      - '/analysis/gitlab-ci/static.yml'
      - '/analysis/gitlab-ci/tests.yml'
      - '/docs/gitlab-ci/commits.yml'


# We use the following two job templates because we build with different images on master and
# stable branches and GitLab CI does allow to set the image name only through variable expansion.
.common-master: &common-master
  rules:
    - if: '$COVERITY_SCAN_RUN'
      when: never
    - if: '$IMAGE_VERSION != "master"'
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success

.common-stable: &common-stable
  rules:
    - if: '$COVERITY_SCAN_RUN'
      when: never
    - if: '$IMAGE_VERSION == "stable"'
      when: on_success
    - if: $CI_COMMIT_TAG
      variables:
        IMAGE_VERSION: stable
      when: on_success
    - when: never


####################################################################################################
#
# Compliance
#

Message Lint:
  extends: .message-lint
  stage: Compliance

Clang-Format:
  extends: .clang-format
  stage: Compliance
  image: $IMAGE

Reuse:
  stage: Compliance
  image: python
  before_script:
    pip install reuse
  script:
    reuse lint


####################################################################################################
#
# Build
#

Coverity Scan:
  extends: .coverity-scan
  stage: Build
  image: $IMAGE

.common-build: &common-build
  stage: Build
  image: $IMAGE
  script:
    - mkdir build && cd build
    - cmake -DCMAKE_DISABLE_PRECOMPILE_HEADERS=ON
      -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_C_COMPILER=clang
      -DCMAKE_CXX_FLAGS=--coverage -DCMAKE_EXE_LINKER_FLAGS=--coverage
      -DCMAKE_INSTALL_PREFIX:PATH=/usr ../
    - make -j$(nproc)
    - make install -j$(nproc)
  artifacts:
    paths:
      - build
    expire_in: 1 week

Master Build:
  <<: *common-build
  <<: *common-master

Stable Build:
  <<: *common-build
  <<: *common-stable


####################################################################################################
#
# Analysis
#

.common-test:
  extends: .tests-with-coverage
  stage: Analysis
  image: $IMAGE
  variables:
    # Following tests are currently disabled on CI:
    # * lockscreen, modifier-only-shortcut: flaky on CI because the lockscreen greeter process likes to
    #   freeze off. Also see: https://bugreports.qt.io/browse/QTBUG-82911
    # * all others: the GLES2 backend of wlroots can't be run in CI/without GPU at the moment,
    #   see https://gitlab.freedesktop.org/wlroots/wlroots/-/issues/2871
    CTEST_ARGS: "-E 'lockscreen|modifier only shortcut|no crash empty deco|no crash no border\
    |scene opengl|opengl shadow|no crash reinit compositor|buffer size change\
    |no crash aurorae destroy deco|slidingpopups|scripted effects|window open close animation\
    |subspace switching animation|minimize animation'"
    GCOVR_ARGS: --gcov-executable '/usr/bin/llvm-cov gcov' -e tests

Master Tests:
  extends: .common-test
  <<: *common-master
  needs:
    - job: Master Build
      artifacts: true

Stable Tests:
  extends: .common-test
  <<: *common-stable
  needs:
    - job: Stable Build
      artifacts: true


####################################################################################################
#
# Deploy
#

Add Coverage:
  extends: .add-coverage
  stage: Deploy
  variables:
    COVERAGE_JOB: Master Tests

pages:
  extends: .pages-coverage
  stage: Deploy
  needs:
    - job: Master Tests
      artifacts: true
